package com.example.project7.model.weatherOfCity

data class WeatherX(
    val description: String,
    val icon: String,
    val id: Int,
    val main: String
)