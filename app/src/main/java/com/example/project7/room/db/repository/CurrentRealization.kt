package com.example.project7.room.db.repository

import androidx.lifecycle.LiveData
import com.example.project7.room.db.dao.CurrentWeatherDao
import com.example.project7.room.modelForDb.CurrentWeatherModel


class CurrentRealization(private val currentWeatherDAO: CurrentWeatherDao) {
    val currentWeather: LiveData<CurrentWeatherModel>
        get() = currentWeatherDAO.getCurrentWeather()


    suspend fun insertCurrentWeather(currentWeatherModel: CurrentWeatherModel) {
        currentWeatherDAO.insert(currentWeatherModel)
    }


    fun deleteCurrentWeather() {
        currentWeatherDAO.deleteAll()

    }
}